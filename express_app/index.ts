import express = require('express');

// Create a new express application instance
const app: express.Application = express();
const port: number = 3000;

app.get('/', function (req, res) {
    res.send('Privet ափերս!');
});

app.listen(port, function () {
    // tslint:disable-next-line:no-console
    console.log(`Server is running in http://localhost:${port}`);
});